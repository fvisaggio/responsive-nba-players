[
   {
      "id":0,
      "RANK":1,
      "PLAYER_ID":201142,
      "PLAYER":"Kevin Durant",
      "TEAM_ID":1610612760,
      "TEAM_ABBREVIATION":"OKC",
      "TEAM_NAME":"Oklahoma City Thunder",
      "PTS":29.6
   },
   {
      "id":1,
      "RANK":2,
      "PLAYER_ID":2544,
      "PLAYER":"LeBron James",
      "TEAM_ID":1610612748,
      "TEAM_ABBREVIATION":"MIA",
      "TEAM_NAME":"Miami Heat",
      "PTS":27.4
   },
   {
      "id":2,
      "RANK":3,
      "PLAYER_ID":201935,
      "PLAYER":"James Harden",
      "TEAM_ID":1610612745,
      "TEAM_ABBREVIATION":"HOU",
      "TEAM_NAME":"Houston Rockets",
      "PTS":26.8
   },
   {
      "id":3,
      "RANK":4,
      "PLAYER_ID":201566,
      "PLAYER":"Russell Westbrook",
      "TEAM_ID":1610612760,
      "TEAM_ABBREVIATION":"OKC",
      "TEAM_NAME":"Oklahoma City Thunder",
      "PTS":26.7
   }
]