var feed=[
   {
      "id":0,
      "RANK":1,
      "PLAYER_ID":201142,
      "PLAYER":"Kevin Durant",
      "TEAM_ID":1610612760,
      "TEAM_ABBREVIATION":"OKC",
      "TEAM_NAME":"Oklahoma City Thunder",
      "PTS":29.6
   },
   {
      "id":1,
      "RANK":2,
      "PLAYER_ID":2544,
      "PLAYER":"LeBron James",
      "TEAM_ID":1610612748,
      "TEAM_ABBREVIATION":"MIA",
      "TEAM_NAME":"Miami Heat",
      "PTS":27.4
   },
   {
      "id":2,
      "RANK":3,
      "PLAYER_ID":201935,
      "PLAYER":"James Harden",
      "TEAM_ID":1610612745,
      "TEAM_ABBREVIATION":"HOU",
      "TEAM_NAME":"Houston Rockets",
      "PTS":26.8
   },
   {
      "id":3,
      "RANK":4,
      "PLAYER_ID":201566,
      "PLAYER":"Russell Westbrook",
      "TEAM_ID":1610612760,
      "TEAM_ABBREVIATION":"OKC",
      "TEAM_NAME":"Oklahoma City Thunder",
      "PTS":26.7
   }
];
$( document ).ready(function() {
	for (var i=0;i<feed.length;i++)
	{
			console.log(feed[i]);
			el = '<div class="player"> \
			<div class="ppg">'+feed[i]["PTS"]+'</div> \
			<div class="text">Playoff Points Per Game </div> \
			<div class="name">'+feed[i]["PLAYER"]+'</div> \
			<div class="rank">#'+feed[i]["RANK"]+' in Points Per Game</div> \
			<div class="team">'+feed[i]["TEAM_NAME"]+'</div> \
			<img class="team-picture" src="http://stats.nba.com/media/logos/'+feed[i]["TEAM_ABBREVIATION"]+'_96x96.png"> \
			<img class="player-picture" src="http://stats.nba.com/media/players/132x132/'+feed[i]["PLAYER_ID"]+'.png"> \
		</div>';
			console.log(el);
			$("#player-container").append(el);
	}
});